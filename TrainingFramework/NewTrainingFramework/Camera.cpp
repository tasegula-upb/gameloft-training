#pragma once
#include "stdafx.h"
#include "Camera.hpp"

#include "Math.h"


Camera::Camera()
{
	position = Vector3(0, 0, 1);
	up = Vector3(0, 1, 0);
	target = Vector3(0, 0, 0);

	m_fov = (GLfloat)PI / 4.0f;
	m_near = (GLfloat) 0.2f;
	m_far = (GLfloat) 500.0f;

	speed = 7;

	mWorld.SetIdentity();
	setView();
	mPersp.SetPerspective(m_fov, Globals::screenWidth / Globals::screenHeight, m_near, m_far);
}

Camera::Camera(Vector3 _position, Vector3 _up, Vector3 _target)
{
	position = _position;
	up = _up;
	target = _target;

	speed = 7;
	deltaTime = 0.0f;

	m_fov = (GLfloat)PI / 4.0f;
	m_near = (GLfloat) 0.2f;
	m_far = (GLfloat) 500.0f;

	mWorld.SetIdentity();
	setView();
	mPersp.SetPerspective(m_fov, Globals::screenWidth / Globals::screenHeight, m_near, m_far);
}


void Camera::setDT(GLfloat _deltaTime)
{
	deltaTime = _deltaTime;
}

void Camera::rotateX(GLfloat alpha)
{
	Matrix mRot, mWorld;
	mRot.SetRotationX(alpha * deltaTime);
	mWorld = getWorld();

	// calculez noul target
	Vector4 localTarget(0, 0, -(position - target).Length(), 1),
		newLocalTarget,
		worldTarget;

	newLocalTarget = localTarget * mRot;
	worldTarget = newLocalTarget * mWorld;

	// reactualizez target-ul
	target.x = worldTarget.x;
	target.y = worldTarget.y;
	target.z = worldTarget.z;

	// calculez noul up
	Vector4 newUp(0, 1, 0, 0), worldUp;

	newUp = newUp * mRot;
	worldUp = (newUp * mWorld).Normalize();

	// actualizez up-ul
	up.x = worldUp.x;
	up.y = worldUp.y;
	up.z = worldUp.z;
}

void Camera::rotateY(GLfloat alpha)
{
	Matrix mRot, mWorld;
	mRot.SetRotationY(alpha * deltaTime);
	mWorld = getWorld();

	// calculez noul target
	Vector4 localTarget(0, 0, -(position - target).Length(), 1),
		newLocalTarget,
		worldTarget;

	newLocalTarget = localTarget * mRot;
	worldTarget = newLocalTarget * mWorld;

	// reactualizez target-ul
	target.x = worldTarget.x;
	target.y = worldTarget.y;
	target.z = worldTarget.z;

	// THIS IS Y - UP stays the same
}

void Camera::rotateZ(GLfloat alpha)
{
	Matrix mRot, mWorld;
	mRot.SetRotationZ(alpha * deltaTime);
	mWorld = getWorld();

	// THIS IS Z - target stays the same ????;
	// calculez noul target
	Vector4 localTarget(0, 0, -(position - target).Length(), 1),
		newLocalTarget,
		worldTarget;
	
	newLocalTarget = localTarget * mRot;
	worldTarget = newLocalTarget * mWorld;

	// reactualizez target-ul
	target.x = worldTarget.x;
	target.y = worldTarget.y;
	target.z = worldTarget.z;

	// calculez noul up
	Vector4 newUp(0, 1, 0, 0), worldUp;

	newUp = newUp * mRot;
	worldUp = (newUp * mWorld).Normalize();

	// actualizez up-ul
	up.x = worldUp.x;
	up.y = worldUp.y;
	up.z = worldUp.z;
}


void Camera::translateForward(GLfloat direction)
{
	Vector3 dist, axa_z;

	axa_z = (position - target).Normalize();
	dist = axa_z * (speed * deltaTime) * (direction > 0.0f ? 1.0f : -1.0f);

	position += dist;
	target += dist;
}

void Camera::translateUp(GLfloat direction)
{
	Vector3 dist, axa_x, axa_y, axa_z;

	axa_z = (position - target).Normalize();
	axa_x = (up.Cross(axa_z)).Normalize();
	axa_y = (axa_z.Cross(axa_x)).Normalize();

	dist = axa_y * (speed * deltaTime) * (direction > 0.0f ? 1.0f : -1.0f);

	position += dist;
	target += dist;
}

void Camera::translateSide(GLfloat direction)
{
	Vector3 dist, axa_x, axa_z;

	axa_z = (position - target).Normalize();
	axa_x = (up.Cross(axa_z)).Normalize();

	dist = axa_x * (speed * deltaTime) * (direction > 0.0f ? 1.0f : -1.0f);

	position += dist;
	target += dist;
}

void Camera::setView()
{
	Vector3 axa_x, axa_y, axa_z;

	axa_z = (position - target).Normalize();
	axa_x = (up.Cross(axa_z)).Normalize();
	axa_y = (axa_z.Cross(axa_x)).Normalize();

	mView.m[0][0] = axa_x.x;		mView.m[0][1] = axa_y.x;		mView.m[0][2] = axa_z.x;		mView.m[0][3] = 0;
	mView.m[1][0] = axa_x.y;		mView.m[1][1] = axa_y.y;		mView.m[1][2] = axa_z.y;		mView.m[1][3] = 0;
	mView.m[2][0] = axa_x.z;		mView.m[2][1] = axa_y.z;		mView.m[2][2] = axa_z.z;		mView.m[2][3] = 0;
	mView.m[3][0] = -position.Dot(axa_x);	mView.m[3][1] = -position.Dot(axa_y);	mView.m[3][2] = -position.Dot(axa_z);	mView.m[3][3] = 1;
}

Matrix Camera::getView()
{
	setView();
	return mView;
}

void Camera::setWorld()
{
	Vector3 axa_x, axa_y, axa_z;

	axa_z = (position - target).Normalize();
	axa_x = (up.Cross(axa_z)).Normalize();
	axa_y = (axa_z.Cross(axa_x)).Normalize();

	mWorld.m[0][0] = axa_x.x;	mWorld.m[0][1] = axa_x.y;	mWorld.m[0][2] = axa_x.z;	mWorld.m[0][3] = 0;
	mWorld.m[1][0] = axa_y.x;	mWorld.m[1][1] = axa_y.y;	mWorld.m[1][2] = axa_y.z;	mWorld.m[1][3] = 0;
	mWorld.m[2][0] = axa_z.x;	mWorld.m[2][1] = axa_z.y;	mWorld.m[2][2] = axa_z.z;	mWorld.m[2][3] = 0;

	mWorld.m[3][0] = position.x;
	mWorld.m[3][1] = position.y;
	mWorld.m[3][2] = position.z;
	mWorld.m[3][3] = 1;
}

Matrix Camera::getWorld()
{
	setWorld();
	return mWorld;
}

Matrix Camera::getPerspective()
{
	return mPersp;
}

Camera::~Camera() {}