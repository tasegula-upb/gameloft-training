#pragma once
#include "stdafx.h"
#include "Object.hpp"

#include "xmldefines.hpp"

HRESULT Object::parseXml(IXmlReader* pReader,
                         IStream* pFileStream,
                         std::unordered_map<GLushort, Object> map)
{
	HRESULT hr = S_OK;
	XmlNodeType nodeType;
	const WCHAR* pPrefix;
	const WCHAR* pLocalName;
	UINT cPrefix;

	Object unknown;

	while (S_OK == (hr = pReader->Read(&nodeType))) {
		switch (nodeType) {
		case XmlNodeType_Whitespace:
			break;
		case XmlNodeType_Element:

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Element[%d]: %s\n", resource_t::MODEL, pLocalName);
			unknown = Object::writeAttributes(pReader);
			map[unknown.ID] = unknown;

			if (pReader->IsEmptyElement()) {
				wprintf(L" (empty)");
			}
			break;

		case XmlNodeType_EndElement:
			if (FAILED(hr = pReader->GetPrefix(&pPrefix, &cPrefix))) {
				wprintf(L"Error getting prefix, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"End Element[%d]: %s\n", resource_t::MODEL, pLocalName);
			return hr;
		default:
			return hr;
		}
	}

	// should not be here
	printf("[Model::parseXml()] should not be here\n");
	return hr;
}

Object Object::writeAttributes(IXmlReader* pReader)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	HRESULT hr = pReader->MoveToFirstAttribute();

	Object unknown;

	if (S_OK != hr) {
		wprintf(L"Error moving to first attribute, error is %08.8lx", hr);
		return unknown;
	}
	else {
		while (TRUE) {
			if (!pReader->IsDefault()) {

				pReader->GetLocalName(&pwszLocalName, NULL);
				pReader->GetValue(&pwszValue, NULL);

				wprintf(L"Attr: %s = \"%s\" \n", pwszLocalName, pwszValue);

			}

			if (S_OK != pReader->MoveToNextAttribute())
				break;
		}
	}

	return unknown;
}

