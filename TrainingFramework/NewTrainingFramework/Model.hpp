#pragma once
#include "stdafx.h"

class Model 
{
	friend class ResourceManager;
	friend class SceneManager;

private:
	GLushort ID;
	std::string filepath;

	GLboolean state; // loaded or not

	GLuint gl_vbo, gl_ibo, gl_num;

public:
	Model(GLushort ID, std::string filepath);

	GLushort getID();
	std::string getPath();

	GLboolean isLoaded();

	GLuint vbo();
	GLuint ibo();
	GLuint num();

	// defaults
	Model() {}

private:
	static HRESULT parseXml(IXmlReader* pReader, 
                          IStream* pFileStream, 
                          std::unordered_map<GLushort, Model>);
	static Model writeAttributes(IXmlReader* pReader);
};