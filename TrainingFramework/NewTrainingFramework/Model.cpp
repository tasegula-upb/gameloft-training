#pragma once
#include "stdafx.h"
#include "Model.hpp"
#include "ModelLoader.hpp"

#include "xmldefines.hpp"

Model::Model(GLushort ID, std::string filepath)
{
	this->ID = ID;
	this->filepath = filepath;

	loadModelNfg(std::string(filepath.begin(), filepath.end()), gl_vbo, gl_ibo, gl_num);
}

HRESULT Model::parseXml(IXmlReader* pReader, 
                        IStream* pFileStream, 
                        std::unordered_map<GLushort, Model> map)
{
	HRESULT hr = S_OK;
	XmlNodeType nodeType;
	const WCHAR* pPrefix;
	const WCHAR* pLocalName;
	UINT cPrefix;

	Model unknown;

	while (S_OK == (hr = pReader->Read(&nodeType))) {
		switch (nodeType) {
		case XmlNodeType_Whitespace:
			break;
		case XmlNodeType_Element:

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Element[%d]: %s\n", resource_t::MODEL, pLocalName);
			unknown = Model::writeAttributes(pReader);
			map[unknown.ID] = unknown;

			if (pReader->IsEmptyElement()) {
				wprintf(L" (empty)");
			}
			break;

		case XmlNodeType_EndElement:
			if (FAILED(hr = pReader->GetPrefix(&pPrefix, &cPrefix))) {
				wprintf(L"Error getting prefix, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"End Element[%d]: %s\n", resource_t::MODEL, pLocalName);
			return hr;
		default:
			return hr;
		}
	}

	// should not be here
	printf("[Model::parseXml()] should not be here\n");
	return hr;
}

Model Model::writeAttributes(IXmlReader* pReader)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	HRESULT hr = pReader->MoveToFirstAttribute();

	Model unknown;

	if (S_OK != hr) {
		wprintf(L"Error moving to first attribute, error is %08.8lx", hr);
		return unknown;
	}
	else {
		while (TRUE) {
			if (!pReader->IsDefault()) {

				pReader->GetLocalName(&pwszLocalName, NULL);
				pReader->GetValue(&pwszValue, NULL);

				if (_wcsicmp(pwszLocalName, L"ID") == 0) {
					unknown.ID = _wtoi(pwszValue);
					printf("ID = %d\n", unknown.ID);
				}
				else if (_wcsicmp(pwszLocalName, L"FILE") == 0) {
					std::wstring ws(pwszValue);
					unknown.filepath.assign(ws.begin(), ws.end());
					printf("FILE = %s \n", unknown.filepath.c_str());
				}
				else {
					wprintf(L"Attr: %s = \"%s\" \n", pwszLocalName, pwszValue);
				}
			}

			if (S_OK != pReader->MoveToNextAttribute())
				break;
		}
	}

	return unknown;
}

GLushort Model::getID()
{
	return ID;
}

GLboolean Model::isLoaded()
{
	return state;
}

std::string Model::getPath()
{
	return filepath;
}

GLuint Model::vbo()
{
	return gl_vbo;
}

GLuint Model::ibo()
{
	return gl_ibo;
}

GLuint Model::num()
{
	return gl_num;
}
