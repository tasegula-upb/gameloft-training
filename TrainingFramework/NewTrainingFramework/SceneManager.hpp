#pragma once
#include "stdafx.h"

#include "Object.hpp"
#include "SpecialObject.hpp"
#include "Light.hpp"
#include "Camera.hpp"

class SceneManager {
private:
	static SceneManager*  instance;

	std::unordered_map<GLuint, Object>         objects;
	std::unordered_map<GLuint, SpecialObject>  specialObjects;
	std::unordered_map<GLuint, Light>          lights;

	Camera camera;

	GLushort nObjects, nSpecial, nLights;

	SceneManager(void);
	SceneManager(const std::string filename);

public:
	//	Always returns the same instance
	static SceneManager* getInstance(const std::string filename);

	//	Adds a new object to the objects vector
	void addObject(GLushort ID, Object object);

	//	Returns an Object
	Object getObject(GLushort ID);

	//	Adds a new Special object to the specialObj vector
	void addSpecialObject(GLushort ID, SpecialObject specialObject);

	//	Returns a special object
	SpecialObject getSpecialObject(GLushort ID);

	//	Adds a new light? to the lights vector
	void addLight(GLushort ID, Light light);

	//	Returns a light ... object?
	Light getLight(GLushort ID);
	
	~SceneManager();

private:
	HRESULT parseXml(std::string filepath);
	HRESULT _parseXml(IXmlReader* pReader, IStream* pFileStream);
	HRESULT writeAttributes(IXmlReader* pReader);

	SceneManager(SceneManager const&);       // Don't Implement
	void operator=(SceneManager const&);     // Don't implement
};

//	some static stuff
SceneManager*  SceneManager::instance;