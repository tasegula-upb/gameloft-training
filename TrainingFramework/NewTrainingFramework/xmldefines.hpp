#pragma warning(disable : 4127)  // conditional expression is constant

#define CLEANUP(hr, stream, reader) SAFE_RELEASE(stream); SAFE_RELEASE(reader); return hr;

#define CHKHR(hr, stream, reader)                     \
	do {                                                \
	if (FAILED(hr)) { CLEANUP(hr, stream, reader); }  \
	} while(0)

#define HR(stmt, stream, reader)                    \
	do { hr = (stmt); CLEANUP(stmt, stream, reader);  \
	} while (0)

#define SAFE_RELEASE(I)         do { if (I){ I->Release(); } I = NULL; } while(0)

enum resource_t {
	NONE,			// No specific type
	MODEL,
	TEXTURE,	// Generic Texture; not used
	TEXTURE2D,
	CUBE_TEXTURE,
	SHADER
};

/*
HRESULT writeAttributesG(IXmlReader* pReader)
{
	const WCHAR* pwszPrefix;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	HRESULT hr = pReader->MoveToFirstAttribute();

	if (S_FALSE == hr) {
		return hr;
	}

	if (S_OK != hr) {
		wprintf(L"Error moving to first attribute, error is %08.8lx", hr);
		return hr;
	}
	else {
		while (TRUE) {
			if (!pReader->IsDefault()) {
				UINT cwchPrefix;

				if (FAILED(hr = pReader->GetPrefix(&pwszPrefix, &cwchPrefix))) {
					wprintf(L"Error getting prefix, error is %08.8lx", hr);
					return hr;
				}

				if (FAILED(hr = pReader->GetLocalName(&pwszLocalName, NULL))) {
					wprintf(L"Error getting local name, error is %08.8lx", hr);
					return hr;
				}

				if (FAILED(hr = pReader->GetValue(&pwszValue, NULL))) {
					wprintf(L"Error getting value, error is %08.8lx", hr);
					return hr;
				}

				if (cwchPrefix > 0) {
					wprintf(L"Attr: %s:%s=\"%s\" \n", pwszPrefix, pwszLocalName, pwszValue);
				}
				else {
					wprintf(L"Attr: %s=\"%s\" \n", pwszLocalName, pwszValue);
				}
			}

			if (S_OK != pReader->MoveToNextAttribute())
				break;
		}
	}
	return hr;
}

HRESULT _parseXmlG(IXmlReader* pReader, IStream* pFileStream)
{
	HRESULT hr = S_OK;
	XmlNodeType nodeType;
	const WCHAR* pPrefix;
	const WCHAR* pLocalName;
	const WCHAR* pValue;
	UINT cPrefix;

	resource_t resource = NONE;

	//read until there are no more nodes
	while (S_OK == (hr = pReader->Read(&nodeType))) {
		switch (nodeType) {
		case XmlNodeType_XmlDeclaration:
		case XmlNodeType_Whitespace:
		case XmlNodeType_DocumentType:
		case XmlNodeType_CDATA:
		case XmlNodeType_ProcessingInstruction:
			break;

		case XmlNodeType_Element:
			if (FAILED(hr = pReader->GetPrefix(&pPrefix, &cPrefix))) {
				wprintf(L"Error getting prefix, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (resource == NONE) {
				if (_wcsicmp(pLocalName, L"Models") == 0) {
					resource = MODEL;
				}
				else if (_wcsicmp(pLocalName, L"Textures2D") == 0)  {
					resource = TEXTURE2D;
				}
				else if (_wcsicmp(pLocalName, L"CubeTextures") == 0) {
					resource = CUBE_TEXTURE;
				}
				else if (_wcsicmp(pLocalName, L"Shaders") == 0) {
					resource = SHADER;
				}
				else printf("\n------------------\nUNKNOWN\n------------------\n");
			}

			if (cPrefix > 0) {
				wprintf(L"Element: %s:%s\n", pPrefix, pLocalName);
			}
			else {
				wprintf(L"Element[%d]: %s\n", resource, pLocalName);
			}

			if (FAILED(hr = writeAttributesG(pReader))) {
				wprintf(L"Error writing attributes, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (pReader->IsEmptyElement()) {
				wprintf(L" (empty)");
			}
			break;

		case XmlNodeType_EndElement:
			if (FAILED(hr = pReader->GetPrefix(&pPrefix, &cPrefix))) {
				wprintf(L"Error getting prefix, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			resource = NONE;

			if (cPrefix > 0) {
				wprintf(L"End Element: %s:%s\n", pPrefix, pLocalName);
			}
			else {
				wprintf(L"End Element[%d]: %s\n", resource, pLocalName);
			}

			break;

		case XmlNodeType_Text:
			if (FAILED(hr = pReader->GetValue(&pValue, NULL))) {
				wprintf(L"Error getting value, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Text: >%s<\n", pValue);
			break;

		case XmlNodeType_Comment:
			if (FAILED(hr = pReader->GetValue(&pValue, NULL))) {
				wprintf(L"Error getting value, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Comment: %s\n", pValue);
			break;

		default:
			break;
		}
	}
	return hr;
}

HRESULT parseXmlG(std::string filepath)
{
	HRESULT hr = S_OK;
	IStream *pFileStream = NULL;
	IXmlReader *pReader = NULL;

	if (FAILED(hr = SHCreateStreamOnFile(filepath.c_str(), STGM_READ, &pFileStream))) {
		wprintf(L"Error creating file reader, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	if (FAILED(hr = CreateXmlReader(__uuidof(IXmlReader), (void**)&pReader, NULL))) {
		wprintf(L"Error creating xml reader, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	if (FAILED(hr = pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit))) {
		wprintf(L"Error setting XmlReaderProperty_DtdProcessing, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	if (FAILED(hr = pReader->SetInput(pFileStream))) {
		wprintf(L"Error setting input for reader, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	return _parseXmlG(pReader, pFileStream);
}
//*/