// NewTrainingFramework.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../Utilities/utilities.h" // if you use STL, please include this line AFTER all other include

#include <conio.h>

#include "Shader.hpp"
#include "Globals.hpp"
#include "Vertex.hpp"
#include "Camera.hpp"
#include "ModelLoader.hpp"

#include "ResourceManager.hpp"
#include "SceneManager.hpp"

GLuint vboId;
Shaders myShaders;
GLuint vbo, ibo, num;
GLuint tex;
Camera c;
int error = 0;
int width, height, bpp;

int Init ( ESContext *esContext )
{
	glClearColor ( 0.0f, 0.0f, 0.0f, 0.0f );
	
	//creation of shaders and program 
	int ret = myShaders.Init("../Resources/Shaders/TriangleShaderVS.vs", "../Resources/Shaders/TriangleShaderFS.fs");

	vbo = ibo = 0;
	loadModelNfg("../../ResourcesPacket/Models/Woman1.nfg", vbo, ibo, num);

	char* tga = LoadTGA("../../ResourcesPacket/Textures/Woman1.tga", &width, &height, &bpp);

	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, (bpp == 32) ? GL_RGBA : GL_RGB, width, height, 0, (bpp == 32) ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, tga);
	glBindTexture(GL_TEXTURE_2D, 0);

	ResourceManager* rp = ResourceManager::getInstance("../Resources/ResourceManager.xml");
	std::cout << "\n\n\n";
	SceneManager* sm = SceneManager::getInstance("../Resources/SceneManager.xml");

	return ret;
}

void Draw(ESContext *esContext)
{
	Matrix world;
	world.SetIdentity();
	Matrix view = c.getView();
	Matrix persp = c.getPerspective();
	Matrix trans = world * view * persp;

	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(myShaders.program);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

	if (myShaders.textureUniform != -1) {
		glUniform1i(myShaders.textureUniform, 0);
	}

	if (myShaders.positionAttribute != -1) {
		glEnableVertexAttribArray(myShaders.positionAttribute);
		glVertexAttribPointer(myShaders.positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, pos)));
	}

	if (myShaders.normalAttribute != -1) {
		glEnableVertexAttribArray(myShaders.normalAttribute);
		glVertexAttribPointer(myShaders.normalAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, norm)));
	}

	if (myShaders.binormalAttribute != -1) {
		glEnableVertexAttribArray(myShaders.binormalAttribute);
		glVertexAttribPointer(myShaders.binormalAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, binorm)));
	}

	if (myShaders.tangentAttribute != -1) {
		glEnableVertexAttribArray(myShaders.tangentAttribute);
		glVertexAttribPointer(myShaders.tangentAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, tgt)));
	}

	if (myShaders.texcoordAttribute != -1) {
		glEnableVertexAttribArray(myShaders.texcoordAttribute);
		glVertexAttribPointer(myShaders.texcoordAttribute, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, uv)));
	}

	if (myShaders.colorAttribute != -1) {
		glEnableVertexAttribArray(myShaders.colorAttribute);
		glVertexAttribPointer(myShaders.colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)(offsetof(Vertex, col)));
	}

	if (myShaders.MVPuniform != -1) {
		glUniformMatrix4fv(myShaders.MVPuniform, 1, GL_FALSE, (GLfloat *)trans.m);
	}

	glDrawElements(GL_TRIANGLES, num, GL_UNSIGNED_SHORT, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	eglSwapBuffers (esContext->eglDisplay, esContext->eglSurface);

}

void Update ( ESContext *esContext, float deltaTime )
{
	c.setDT(deltaTime);
}

void Key ( ESContext *esContext, unsigned char key, bool bIsPressed)
{
	GLfloat pas = 15.0f;
	if (bIsPressed) {
		switch (key) {
			case 'w':	case 'W':		c.translateForward(-1);	break;
			case 's':	case 'S':	  c.translateForward(+1);	break;
			case 'a':	case 'A':	  c.translateSide(+1);		break;
			case 'd':	case 'D':	  c.translateSide(-1);		break;
			case 'q':	case 'Q':	  c.translateUp(+1);			break;
			case 'e':	case 'E':	  c.translateUp(-1);			break;

			case 'z':	case 'Z':	  c.rotateX(+pas);	break;
			case 'x':	case 'X':	  c.rotateX(-pas);	break;
			case 'r':	case 'R':	  c.rotateY(+pas);	break;
			case 'f':	case 'F':	  c.rotateY(-pas);	break;
			case 'c':	case 'C':	  c.rotateZ(+pas);	break;
			case 'v':	case 'V':	  c.rotateZ(-pas);	break;
		
		default:
			break;
		}
	}
}

void CleanUp()
{
	glDeleteBuffers(1, &vboId);
}

int _tmain(int argc, _TCHAR* argv[])
{
	//identifying memory leaks
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF ); 

	ESContext esContext;
    esInitContext ( &esContext );
	esCreateWindow ( &esContext, "Hello Triangle", Globals::screenWidth, Globals::screenHeight, ES_WINDOW_RGB | ES_WINDOW_DEPTH);

	if ( Init ( &esContext ) != 0 )
		return 0;

	esRegisterDrawFunc ( &esContext, Draw );
	esRegisterUpdateFunc ( &esContext, Update );
	esRegisterKeyFunc ( &esContext, Key);

	esMainLoop ( &esContext );

	//releasing OpenGL resources
	CleanUp();

	printf("Press any key...\n");
	_getch();
		
	return 0;
}

