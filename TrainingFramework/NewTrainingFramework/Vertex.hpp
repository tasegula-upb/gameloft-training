#pragma once
#include "Math.h"

struct Vertex
{
	Vector3 pos;     // position
	Vector3 norm;    // normal
	Vector3 binorm;  // binormal
	Vector3 tgt;     // tangent
	Vector2 uv;      // texture coordinates
	Vector3 col;     // color

	Vertex() : pos(0, 0, 0), norm(0, 0, 0), binorm(0, 0, 0), tgt(0, 0, 0), uv(0, 0), col(0, 0, 0) { }

	Vertex(Vector3 _pos) : norm(0, 0, 0), binorm(0, 0, 0), tgt(0, 0, 0), uv(0, 0), col(0, 0, 0)
	{
		pos = _pos;
	}

	Vertex(Vector3 _pos, Vector3 _norm) : binorm(0, 0, 0), tgt(0, 0, 0), uv(0, 0), col(0, 0, 0)
	{
		pos = _pos;
		norm = _norm;
	}

	Vertex(Vector3 _pos, Vector2 _uv) : norm(0, 0, 0), binorm(0, 0, 0), tgt(0, 0, 0), col(0, 0, 0)
	{
		pos = _pos;
		uv = _uv;
	}

	Vertex(Vector3 _pos, Vector3 _norm, Vector2 _uv) : binorm(0, 0, 0), tgt(0, 0, 0), col(0, 0, 0)
	{
		pos = _pos;
		norm = _norm;
		uv = _uv;
	}

	Vertex(Vector3 _pos, Vector3 _norm, Vector3 _col) : binorm(0, 0, 0), tgt(0, 0, 0), uv(0, 0)
	{
		pos = _pos;
		norm = _norm;
		col = _col;
	}

	Vertex(Vector3 _pos, Vector3 _norm, Vector3 _binorm, Vector3 _tgt, Vector2 _uv) : col(0, 0, 0)
	{
		pos = _pos;
		norm = _norm;
		binorm = _binorm;
		tgt = _tgt;
		uv = _uv;
	}

	Vertex(Vector3 _pos, Vector3 _norm, Vector3 _binorm, Vector3 _tgt, Vector3 _col) : uv(0, 0)
	{
		pos = _pos;
		norm = _norm;
		binorm = _binorm;
		tgt = _tgt;
		col = _col;
	}

	Vertex(Vector3 _pos, Vector3 _norm, Vector3 _binorm, Vector3 _tgt, Vector2 _uv, Vector3 _col)
	{
		pos = _pos;
		norm = _norm;
		binorm = _binorm;
		tgt = _tgt;
		uv = _uv;
		col = _col;
	}
};