#pragma once
#include "stdafx.h"

class Shaders
{
	friend class ResourceManager;
	friend class SceneManager;

public:
	GLuint ID = 0;
	GLuint program, vertexShader, fragmentShader;

	std::string fileVS;
	std::string fileFS;

	GLuint positionAttribute;
	GLuint normalAttribute;
	GLuint binormalAttribute;
	GLuint tangentAttribute;
	GLuint texcoordAttribute;
	GLuint colorAttribute;

	GLint MVPuniform;
	GLuint textureUniform;

	Shaders(char* fileVertexShader, char* fileFragmentShader);
	int Init(char* fileVertexShader, char* fileFragmentShader);

	// defaults
	Shaders() {}
	~Shaders();		// implemented

private:
	static HRESULT parseXml(IXmlReader* pReader, IStream* pFileStream, std::unordered_map<GLushort, Shaders>);
	static Shaders writeAttributes(IXmlReader* pReader);
};