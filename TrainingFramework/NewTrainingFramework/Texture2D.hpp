#pragma once
#include "stdafx.h"

class Texture2D 
{
	friend class ResourceManager;
	friend class SceneManager;

private:
	GLushort ID;
	std::string filepath;

	GLboolean state; // loaded or not

	GLuint texbind;

public:
	Texture2D(GLushort ID, std::string filepath);

	GLushort getID();
	std::string getPath();

	GLboolean isLoaded();

	GLuint texBind();

	// defaults
	Texture2D() {}

private:
	static HRESULT parseXml(IXmlReader* pReader, IStream* pFileStream, std::unordered_map<GLushort, Texture2D>);
	static Texture2D writeAttributes(IXmlReader* pReader);
};