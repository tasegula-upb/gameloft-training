#pragma once
#include "stdafx.h"

#include "Shader.hpp"
#include "Model.hpp"
#include "Texture2D.hpp"
#include "TextureCube.hpp"


class ResourceManager {
private:
	static ResourceManager*  instance;

	std::unordered_map<GLushort, Shaders>      shaders;
	std::unordered_map<GLushort, Model>        models;
	std::unordered_map<GLushort, Texture2D>    textures;
	std::unordered_map<GLushort, TextureCube>  cubetex;

	GLushort nShaders, nModels, nTextures, nCubeTex;

	ResourceManager(void);
	ResourceManager(const std::string filename);

public:
	//	Always returns the same instance
	static ResourceManager* getInstance(const std::string filename);

	//	Adds a new model to the models vector
	void addModel(GLushort ID, Model model);
	
	//	Returns a model
	Model getModel(GLushort ID);

	//	Adds a new shader to the shaders vector
	void addShader(GLushort ID, Shaders shader);

	//	Returns a shader
	Shaders getShader(GLushort ID);

	//	Adds a new texture 2D to the textures vector
	void addTexture(GLushort ID, Texture2D texture);

	//	Returns a texture2D
	Texture2D getTexture(GLushort ID);

	//	Adds a new cube texture to the cubetex vector
	void addCubeTexture(GLushort ID, TextureCube texture);

	//	Returns a shader
	TextureCube getCubeTexture(GLushort ID);

	~ResourceManager();

private:
	HRESULT parseXml(std::string filepath);
	HRESULT _parseXml(IXmlReader* pReader, IStream* pFileStream);
	HRESULT writeAttributes(IXmlReader* pReader);

	ResourceManager(ResourceManager const&);    // Don't Implement
	void operator=(ResourceManager const&);     // Don't implement

};
