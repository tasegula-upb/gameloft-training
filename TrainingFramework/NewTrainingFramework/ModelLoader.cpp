#pragma once
#include "stdafx.h"
#include "ModelLoader.hpp"

#include "Shader.hpp"

/*
 *	Functions used for parsing the model object
 */

//	loads only the geometry from the file
void _loadModelNfg(const std::string &filename, std::vector<Vertex> &vertices, std::vector<GLushort> &indices);

//	writes the tokens of the source string into tokens
void _stringTokenize(const std::string &source, std::vector<std::string> &tokens);

//	converts a string to ...
GLfloat _stringToFloat(const std::string &source);
GLuint _stringToUint(const std::string &source);
GLint _stringToInt(const std::string &source);
GLushort _stringToUshort(const std::string &source);
GLshort _strintToShort(const std::string &source);


/*
 *	THE MAIN FUNCTION; THE ONE THAT WILL BE CALLED
 */
void loadModelNfg(const std::string &filename, GLuint &vbo, GLuint &ibo, GLuint &num)
{
	// put here the verteces and indices from the file
	std::vector<Vertex> vertices;
	std::vector<GLushort> indices;

	_loadModelNfg(filename, vertices, indices);
	wprintf(L"Mesh Loader: loaded file: %s\n", filename);

	//*
	std::cout << "Vertex:\n";
	for (unsigned int i = 0; i < vertices.size(); i++) {
		printf("Vertex No: %d\n"
			"pos:    (%3.6f %3.6f %3.6f)\n"
			"norm:   (%3.6f %3.6f %3.6f)\n"
			"binorm: (%3.6f %3.6f %3.6f)\n"
			"tgt:    (%3.6f %3.6f %3.6f)\n"
			"uv:     (%3.6f %3.6f)\n"
			"col:    (%3.6f %3.6f %3.6f)\n\n",
			i,
			vertices[i].pos.x, vertices[i].pos.y, vertices[i].pos.z,
			vertices[i].norm.x, vertices[i].norm.y, vertices[i].norm.z,
			vertices[i].binorm.x, vertices[i].binorm.y, vertices[i].binorm.z,
			vertices[i].tgt.x, vertices[i].tgt.y, vertices[i].tgt.z,
			vertices[i].uv.x, vertices[i].uv.y,
			vertices[i].col.x, vertices[i].col.y, vertices[i].col.z);
	}
	//*/

	// creates OpenGL objects necessary for drawing
	GLuint gl_vertex_buffer_object, gl_index_buffer_object;

	// vertex buffer object -> object in which to keep the vertices
	glGenBuffers(1, &gl_vertex_buffer_object);
	glBindBuffer(GL_ARRAY_BUFFER, gl_vertex_buffer_object);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// index buffer object -> object in which to keep the indices
	glGenBuffers(1, &gl_index_buffer_object);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_index_buffer_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	vbo = gl_vertex_buffer_object;
	ibo = gl_index_buffer_object;
	num = indices.size();
}


void _loadModelNfg(const std::string &filename, std::vector<Vertex> &vertices, std::vector<GLushort> &indices)
{
	//	read from file
	std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
	if (!file.good()){
		printf("Model Loader: I didn't find the file %s or I didn't have the right to open it!\n", filename);
		std::terminate();
	}

	std::string line;
	std::vector<std::string> tokens;

	unsigned int i = 0, j = 0;
	unsigned int verticesNo = 0;
	unsigned int indicesNo = 0;

	//	tokenize the readed line to get the number of vertices;
	std::getline(file, line);
	_stringTokenize(line, tokens);

	if (tokens.size() != 2) return;
	verticesNo = _stringToUshort(tokens[1]);
	vertices.reserve(verticesNo);

	Vector3 pos, norm, binorm, tgt;
	Vector2 uv;

	for (i = 0; i < verticesNo; i++) {
		//	read line
		std::getline(file, line);

		//	tokenize the readed line
		_stringTokenize(line, tokens);

		//	if there is no token, continue
		if (tokens.size() == 0) {
			std::cout << "vertices: Empty line\n";
			continue;
		}
		if (tokens.size() != 20) {
			std::cout << "only supports this nfg format: \n";
			std::cout << "index pos [x, y, z], norm [x, y, z], binorm [x, y, z], tgt [x, y, z], uv [u, v]\n";
		}

		if (i != _stringToUshort(tokens[0])) {
			printf("ERROR at reading vertices: expected %d, got %d\n", i, _stringToUshort(tokens[0]));
			break;
		}

		// ID. pos [x y z], norm [x y z], binorm [x y z], tgt [x y z], uv [u v]
		// 0   1    2 3 4   5     6  7 8  9       0 1 2   3    4 5 6   7   8 9
		pos    = Vector3(_stringToFloat(tokens[2]), _stringToFloat(tokens[3]), _stringToFloat(tokens[4]));
		norm   = Vector3(_stringToFloat(tokens[6]), _stringToFloat(tokens[7]), _stringToFloat(tokens[8]));
		binorm = Vector3(_stringToFloat(tokens[10]), _stringToFloat(tokens[11]), _stringToFloat(tokens[12]));
		tgt    = Vector3(_stringToFloat(tokens[14]), _stringToFloat(tokens[15]), _stringToFloat(tokens[16]));
		uv     = Vector2(_stringToFloat(tokens[18]), _stringToFloat(tokens[19]));

		vertices.push_back(Vertex(pos, norm, binorm, tgt, uv));
	}

	//	tokenize the readed line to get the number of faces;
	std::getline(file, line);
	_stringTokenize(line, tokens);

	if (tokens.size() != 2) return;
	indicesNo = _stringToUshort(tokens[1]) / 3;
	indices.reserve(indicesNo);

	for (i = 0; i < indicesNo; i++) {
		//	read line
		std::getline(file, line);

		//	tokenize the readed line
		_stringTokenize(line, tokens);

		//	if there is no token, continue
		if (tokens.size() == 0) {
			std::cout << "indices: Empty line\n";
			continue;
		}
		if (tokens.size() != 4) {
			std::cout << "only supports this nfg format: \n";
			std::cout << "index v1 v2 v3\n";
		}

		if (i != _stringToUshort(tokens[0])) {
			printf("ERROR at reading indices: expected %d, got %d\n", i, _stringToUshort(tokens[0]));
			break;
		}

		indices.push_back(_stringToUshort(tokens[1]));
		indices.push_back(_stringToUshort(tokens[2]));
		indices.push_back(_stringToUshort(tokens[3]));
	}
}


//	writes the tokens of the source string into tokens
void _stringTokenize(const std::string &source, std::vector<std::string> &tokens)
{
	tokens.clear();
	unsigned int i = 0;
	std::string aux = source;

	if (source.size() == 0) return;

	//	the first token is the vertex number; get rid of the '.'
	if (aux[0] != 'N') {
		while (aux[i] != '.') {
			i++;
		}
		aux[i] = ' ';
		i++;
	}

	// already have the i set
	for (i = 0; i < aux.size(); i++) {
		if (aux[i] == ',' || aux[i] == ':' || aux[i] == ';' ||
			aux[i] == '[' || aux[i] == ']' ||
			aux[i] == '\t' || aux[i] == '\n')
		{
			aux[i] = ' ';
		}
	}

	std::stringstream ss(aux, std::ios::in);

	while (ss.good()) {
		std::string s;
		ss >> s;
		if (s.size()>0) tokens.push_back(s);
	}
}


GLfloat _stringToFloat(const std::string &source)
{
	std::stringstream ss(source.c_str());
	GLfloat result;
	ss >> result;
	return result;
}

GLuint _stringToUint(const std::string &source)
{
	std::stringstream ss(source.c_str());
	GLuint result;
	ss >> result;
	return result;
}

GLint _stringToInt(const std::string &source)
{
	std::stringstream ss(source.c_str());
	GLint result;
	ss >> result;
	return result;
}

GLushort _stringToUshort(const std::string &source)
{
	std::stringstream ss(source.c_str());
	GLushort result;
	ss >> result;
	return result;
}

GLshort _stringToShort(const std::string &source)
{
	std::stringstream ss(source.c_str());
	GLshort result;
	ss >> result;
	return result;
}

