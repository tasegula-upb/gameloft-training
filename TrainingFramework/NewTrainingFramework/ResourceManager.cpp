#pragma once
#include "stdafx.h"
#include "ResourceManager.hpp"
#include "xmldefines.hpp"

//	some static stuff
ResourceManager* ResourceManager::instance = NULL;

ResourceManager::ResourceManager(void)
{
	nShaders = nModels = nTextures = nCubeTex = 0;
}

ResourceManager::ResourceManager(const std::string filename)
{
	nShaders = nModels = nTextures = nCubeTex = 0;
	parseXml(filename);
}

//	Always returns the same instance
ResourceManager* ResourceManager::getInstance(const std::string filename)
{
	if (!instance) {
		if (filename != "") {
			instance = new ResourceManager(filename);
		}
		else {
			instance = new ResourceManager();
		}
	}

	return instance;
}

//	Parsing
HRESULT ResourceManager::writeAttributes(IXmlReader* pReader)
{
	const WCHAR* pwszPrefix;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	HRESULT hr = pReader->MoveToFirstAttribute();

	if (S_FALSE == hr) {
		return hr;
	}

	if (S_OK != hr) {
		wprintf(L"Error moving to first attribute, error is %08.8lx", hr);
		return hr;
	}
	else {
		while (TRUE) {
			if (!pReader->IsDefault()) {
				UINT cwchPrefix;

				if (FAILED(hr = pReader->GetPrefix(&pwszPrefix, &cwchPrefix))) {
					wprintf(L"Error getting prefix, error is %08.8lx", hr);
					return hr;
				}

				if (FAILED(hr = pReader->GetLocalName(&pwszLocalName, NULL))) {
					wprintf(L"Error getting local name, error is %08.8lx", hr);
					return hr;
				}

				if (FAILED(hr = pReader->GetValue(&pwszValue, NULL))) {
					wprintf(L"Error getting value, error is %08.8lx", hr);
					return hr;
				}

				if (cwchPrefix > 0) {
					wprintf(L"Attr: %s:%s=\"%s\" \n", pwszPrefix, pwszLocalName, pwszValue);
				}
				else {
					wprintf(L"Attr: %s=\"%s\" \n", pwszLocalName, pwszValue);
				}
			}

			if (S_OK != pReader->MoveToNextAttribute())
				break;
		}
	}
	return hr;
}

HRESULT ResourceManager::_parseXml(IXmlReader* pReader, IStream* pFileStream)
{
	HRESULT hr = S_OK;
	XmlNodeType nodeType;
	const WCHAR* pPrefix;
	const WCHAR* pLocalName;
	const WCHAR* pValue;
	UINT cPrefix;

	resource_t resource = NONE;

	//read until there are no more nodes
	while (S_OK == (hr = pReader->Read(&nodeType))) {
		switch (nodeType) {
		case XmlNodeType_XmlDeclaration:
		case XmlNodeType_Whitespace:
		case XmlNodeType_DocumentType:
		case XmlNodeType_CDATA:
		case XmlNodeType_ProcessingInstruction:
			break;

		case XmlNodeType_Element:

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (resource == NONE) {
				if (_wcsicmp(pLocalName, L"Models") == 0) {
					hr = Model::parseXml(pReader, pFileStream, models);
				}
				else if (_wcsicmp(pLocalName, L"Textures2D") == 0)  {
					hr = Texture2D::parseXml(pReader, pFileStream, textures);
				}
				else if (_wcsicmp(pLocalName, L"CubeTextures") == 0) {
					hr = TextureCube::parseXml(pReader, pFileStream, cubetex);
				}
				else if (_wcsicmp(pLocalName, L"Shaders") == 0) {
					hr = Shaders::parseXml(pReader, pFileStream, shaders);
				}
				else printf("\n------------------\nUNKNOWN\n------------------\n");
			}

			wprintf(L"Element[%d]: %s\n", resource, pLocalName);

			if (FAILED(hr = ResourceManager::writeAttributes(pReader))) {
				wprintf(L"Error writing attributes, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (pReader->IsEmptyElement()) {
				wprintf(L" (empty)");
			}
			break;

		case XmlNodeType_EndElement:
			if (FAILED(hr = pReader->GetPrefix(&pPrefix, &cPrefix))) {
				wprintf(L"Error getting prefix, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			resource = NONE;

			if (cPrefix > 0) {
				wprintf(L"End Element: %s:%s\n", pPrefix, pLocalName);
			}
			else {
				wprintf(L"End Element[%d]: %s\n", resource, pLocalName);
			}

			break;

		case XmlNodeType_Text:
			if (FAILED(hr = pReader->GetValue(&pValue, NULL))) {
				wprintf(L"Error getting value, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Text: >%s<\n", pValue);
			break;

		case XmlNodeType_Comment:
			if (FAILED(hr = pReader->GetValue(&pValue, NULL))) {
				wprintf(L"Error getting value, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Comment: %s\n", pValue);
			break;

		default:
			break;
		}
	}
	return hr;
}

HRESULT ResourceManager::parseXml(std::string filepath)
{
	HRESULT hr = S_OK;
	IStream *pFileStream = NULL;
	IXmlReader *pReader = NULL;

	if (FAILED(hr = SHCreateStreamOnFile(filepath.c_str(), STGM_READ, &pFileStream))) {
		wprintf(L"Error creating file reader, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	if (FAILED(hr = CreateXmlReader(__uuidof(IXmlReader), (void**)&pReader, NULL))) {
		wprintf(L"Error creating xml reader, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	if (FAILED(hr = pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit))) {
		wprintf(L"Error setting XmlReaderProperty_DtdProcessing, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	if (FAILED(hr = pReader->SetInput(pFileStream))) {
		wprintf(L"Error setting input for reader, error is %08.8lx", hr);
		HR(hr, pFileStream, pReader);
	}

	return ResourceManager::_parseXml(pReader, pFileStream);
}



//	Adds a new model to the models vector
void ResourceManager::addModel(GLushort ID, Model model)
{
	models[ID] = model;
	nModels++;
}

//	Returns a model
Model ResourceManager::getModel(GLushort ID)
{
	return models[ID];
}

//	Adds a new shader to the shaders vector
void ResourceManager::addShader(GLushort ID, Shaders shader)
{
	shaders[ID] = shader;
	nShaders++;
}

//	Returns a shader
Shaders ResourceManager::getShader(GLushort ID)
{
	return shaders[ID];
}

//	Adds a new texture 2D to the textures vector
void ResourceManager::addTexture(GLushort ID, Texture2D texture)
{
	textures[ID] = texture;
	nTextures++;
}

//	Returns a texture2D
Texture2D ResourceManager::getTexture(GLushort ID)
{
	return textures[ID];
}

//	Adds a new cube texture to the cubetex vector
void ResourceManager::addCubeTexture(GLushort ID, TextureCube texture)
{
	cubetex[ID] = texture;
	nCubeTex++;
}

//	Returns a shader
TextureCube ResourceManager::getCubeTexture(GLushort ID)
{
	return cubetex[ID];
}

