#include "stdafx.h"
#include "Shader.hpp"
#include "xmldefines.hpp"

Shaders::Shaders(char* fileVertexShader, char* fileFragmentShader)
{
	fileVS = fileVertexShader;
	fileFS = fileFragmentShader;

	Init(fileVertexShader, fileFragmentShader);
}

int Shaders::Init(char* fileVertexShader, char* fileFragmentShader)
{
	vertexShader = esLoadShader(GL_VERTEX_SHADER, fileVertexShader);

	if (vertexShader == 0)
		return -1;

	fragmentShader = esLoadShader(GL_FRAGMENT_SHADER, fileFragmentShader);

	if (fragmentShader == 0) {
		glDeleteShader(vertexShader);
		return -2;
	}
	
	program = esLoadProgram(vertexShader, fragmentShader);

	//finding location of uniforms / attributes
	positionAttribute = glGetAttribLocation(program, "a_posL");
	normalAttribute = glGetAttribLocation(program, "a_normL");
	binormalAttribute = glGetAttribLocation(program, "a_binormL");
	tangentAttribute = glGetAttribLocation(program, "a_tgtL");
	texcoordAttribute = glGetAttribLocation(program, "a_uvL");
	colorAttribute = glGetAttribLocation(program, "a_colL");
	MVPuniform = glGetUniformLocation(program, "uMVP");
	textureUniform = glGetUniformLocation(program, "texture");
	return 0;
}

HRESULT Shaders::parseXml(IXmlReader* pReader, IStream* pFileStream, std::unordered_map<GLushort, Shaders> map)
{
	HRESULT hr = S_OK;
	XmlNodeType nodeType;
	const WCHAR* pPrefix;
	const WCHAR* pLocalName;
	UINT cPrefix;

	Shaders unknown;

	while (S_OK == (hr = pReader->Read(&nodeType))) {
		switch (nodeType) {
		case XmlNodeType_Whitespace:
			break;
		case XmlNodeType_Element:

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"Element[%d]: %s\n", resource_t::SHADER, pLocalName);
			unknown = Shaders::writeAttributes(pReader);
			map[unknown.ID] = unknown;

			if (pReader->IsEmptyElement()) {
				wprintf(L" (empty)");
			}
			break;

		case XmlNodeType_EndElement:
			if (FAILED(hr = pReader->GetPrefix(&pPrefix, &cPrefix))) {
				wprintf(L"Error getting prefix, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			if (FAILED(hr = pReader->GetLocalName(&pLocalName, NULL))) {
				wprintf(L"Error getting local name, error is %08.8lx", hr);
				HR(hr, pFileStream, pReader);
			}

			wprintf(L"End Element[%d]: %s\n", resource_t::SHADER, pLocalName);
			return hr;
		default:
			return hr;
		}
	}

	// should not be here
	printf("[Shaders::parseXml()] should not be here\n");
	return hr;
}

Shaders Shaders::writeAttributes(IXmlReader* pReader)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	HRESULT hr = pReader->MoveToFirstAttribute();

	Shaders unknown;

	if (S_OK != hr) {
		wprintf(L"Error moving to first attribute, error is %08.8lx", hr);
		return unknown;
	}
	else {
		while (TRUE) {
			if (!pReader->IsDefault()) {

				pReader->GetLocalName(&pwszLocalName, NULL);
				pReader->GetValue(&pwszValue, NULL);

				if (_wcsicmp(pwszLocalName, L"ID") == 0) {
					unknown.ID = _wtoi(pwszValue);
					printf("ID = %d\n", unknown.ID);
				}
				else if (_wcsicmp(pwszLocalName, L"VS") == 0) {
					std::wstring ws(pwszValue);
					unknown.fileVS.assign(ws.begin(), ws.end());
					printf("VS = %s \n", unknown.fileVS.c_str());
				}
				else if (_wcsicmp(pwszLocalName, L"FS") == 0) {
					std::wstring ws(pwszValue);
					unknown.fileFS.assign(ws.begin(), ws.end());
					printf("FS = %s \n", unknown.fileFS.c_str());
				}
				else {
					wprintf(L"Attr: %s = \"%s\" \n", pwszLocalName, pwszValue);
				}
			}

			if (S_OK != pReader->MoveToNextAttribute())
				break;
		}
	}

	return unknown;
}

Shaders::~Shaders()
{
	glDeleteProgram(program);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}