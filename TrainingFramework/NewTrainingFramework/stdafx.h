// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

// TODO: reference additional headers your program requires here
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <unordered_map>

#include <shlwapi.h>
#include <ole2.h>
#include <xmllite.h>

#include "../Utilities/utilities.h"

// duno if it's ok, but ...
const double PI = 3.14159265359;