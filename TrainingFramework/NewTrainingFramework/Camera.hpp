#pragma once
#include "stdafx.h"
#include "Math.h"
#include "Globals.hpp"


class Camera
{
	friend class ResourceManager;
	friend class SceneManager;

private:
	Vector3 position;
	Vector3 up;
	Vector3 target;

	GLfloat m_near, m_far, m_fov;

	Matrix mView, mWorld, mPersp;

	GLfloat speed, deltaTime;

public:
	Camera(void);
	Camera(Vector3 _position, Vector3 _up, Vector3 _target);
	~Camera(void);

	void setDT(GLfloat _deltaTime);

	void rotateX(GLfloat alpha);
	void rotateY(GLfloat alpha);
	void rotateZ(GLfloat alpha);

	void rotateTPSoX(GLfloat alpha);
	void rotateTPSoY(GLfloat alpha);
	void rotateTPSoZ(GLfloat alpha);

	void translateForward(GLfloat direction);
	void translateUp(GLfloat direction);
	void translateSide(GLfloat direction);

	void setView();
	Matrix getView();

	void setWorld();
	Matrix getWorld();

	Matrix getPerspective();
};