#pragma once
#include "stdafx.h"

#include "Shader.hpp"
#include "Model.hpp"
#include "Texture2D.hpp"
#include "TextureCube.hpp"
#include "Light.hpp"

class Object 
{
	friend class ResourceManager;
	friend class SceneManager;

private:
	GLushort ID;

	Model        model;
	Shaders      shader;

	std::unordered_map<GLuint, Texture2D>    texture;
	std::unordered_map<GLuint, TextureCube>  cubetex;

	std::unordered_map<GLuint, Light>        lights;

	Vector3 position;
	Vector3 rotation;
	Vector3 scale;

public:
	// defaults
	Object() {}

	

private:
	static HRESULT parseXml(IXmlReader* pReader,
                          IStream* pFileStream,
                          std::unordered_map<GLushort, Object>);
	static Object writeAttributes(IXmlReader* pReader);
};