#pragma once
#include "stdafx.h"
#include "../Utilities/utilities.h"

#include "Model.hpp"
#include "Shader.hpp"
#include "Texture2D.hpp"
#include "TextureCube.hpp"
#include "Light.hpp"

class SpecialObject 
{
	friend class ResourceManager;
	friend class SceneManager;

private:
	GLushort ID;

	Model        model;
	Shaders      shader;
	std::vector<Texture2D>    textures;
	std::vector<TextureCube>  cubetextures;

	GLuint* lights;

	Vector3 position;
	Vector3 rotation;
	Vector3 scale;
};