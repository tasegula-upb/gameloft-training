#pragma once
#include "stdafx.h"

#include "Shader.hpp"
#include "Vertex.hpp"

//	Function used for loading the model
//	returns in vao, vbo and ibo, links to the given model.
void loadModelNfg(const std::string &filename, GLuint &vbo, GLuint &ibo, GLuint &num);

