#pragma once
#include "stdafx.h"

class TextureCube 
{
	friend class ResourceManager;
	friend class SceneManager;

private:
	GLushort ID;
	std::string filepath;

	GLboolean state; // loaded or not

	GLuint texbind;
	
public:
	TextureCube(GLushort ID, std::string filepath);

	GLushort getID();
	std::string getPath();

	GLboolean isLoaded();

	GLuint texBind();

	// Defaults
	TextureCube() {}

private:
	static HRESULT parseXml(IXmlReader* pReader, IStream* pFileStream, std::unordered_map<GLushort, TextureCube>);
	static TextureCube writeAttributes(IXmlReader* pReader);
};