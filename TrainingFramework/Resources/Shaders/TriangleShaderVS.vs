attribute vec3 a_posL;
attribute vec3 a_normL;
attribute vec3 a_binormL;
attribute vec3 a_tgtL;
attribute vec2 a_uvL;
attribute vec3 a_colL;

uniform mat4 uMVP;

//varying vec3 v_color;
varying vec2 v_texcoord;

void main()
{
vec4 posL = vec4(a_posL, 1.0);
vec4 aux = uMVP*  posL;
aux.z = -aux.z;
gl_Position = aux;

//v_color = a_colL;

v_texcoord = a_uvL;
}
